package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private final List<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "spring2"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Company> findByPage(int pageNumber, int pageSize) {
        return companies.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company insert(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }

    public Company updateCompany(Long id, Company company) {
        Company targetCompany = findById(id);
        targetCompany.setName(company.getName());
        return targetCompany;
    }

    public void deleteCompany(Long id) {
        Company targetCompany = findById(id);
        companies.remove(targetCompany);
    }
}
