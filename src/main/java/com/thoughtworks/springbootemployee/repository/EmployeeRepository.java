package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lucy", 20, "Female", 2000, 1L));
        employees.add(new Employee(2L, "Lily", 21, "Female", 30001, 2L));
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findByID(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee addEmployee(Employee employee) {
        Long id = generateID();
        employee.setId(id);
        employees.add(employee);
        return employee;
    }

    private Long generateID() {
        return employees.stream()
                .mapToLong(Employee::getId)
                .max()
                .orElse(0L) + 1;
    }

    public Employee updateEmployee(Long id, Employee employee) {
        Employee targetEmployee = findByID(id);
        targetEmployee.setAge(employee.getAge());
        targetEmployee.setSalary(employee.getSalary());
        return targetEmployee;
    }

    public void deleteEmployee(Long id) {
        Employee targetEmployee = findByID(id);
        employees.remove(targetEmployee);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Employee> findByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> employee.isInCompany(companyId))
                .collect(Collectors.toList());
    }
}
